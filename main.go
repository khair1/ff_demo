package main

import (
	"html/template"
	"log"
	"net/http"

	unleash "github.com/Unleash/unleash-client-go"
)

var (
	GITLAB_URL         = "https://gitlab.com/api/v4/feature_flags/unleash/9225313"
	GITLAB_INSTANCE_ID = "rn15Sa2azJZAeZ9AzvJo"
	GITLAB_APP_NAME    = "production"
)

var tpl *template.Template

type metricsInterface struct {
}

type Person struct {
	Name string
}

func init() {
	unleash.Initialize(
		unleash.WithUrl(GITLAB_URL),
		unleash.WithInstanceId(GITLAB_INSTANCE_ID),
		unleash.WithAppName(GITLAB_APP_NAME),
		unleash.WithListener(&metricsInterface{}),
	)

	tpl = template.Must(template.ParseGlob("templates/*"))
}

func server(w http.ResponseWriter, req *http.Request) {
	p := Person{Name: getName(req)}
	tpl.ExecuteTemplate(w, "index.gohtml", &p)
}

func getName(req *http.Request) string {
	name := ""
	if unleash.IsEnabled("greeting") {
		name = req.URL.Query().Get("name")
	}

	if name == "" {
		name = "World"
	}

	return name
}

func main() {
	http.HandleFunc("/", server)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
